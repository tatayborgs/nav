package com.example.ross.navigationdrawer;



import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class logIn extends AppCompatActivity {

    private EditText editTextUsername;
    private EditText editTextPassword;
    private Button buttonLogIn;
    private TextView textViewSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        editTextUsername=(EditText)findViewById(R.id.editTextUserName);
        editTextPassword=(EditText)findViewById(R.id.editTextPassword);
        buttonLogIn=(Button)findViewById(R.id.buttonLogIn);
        textViewSignUp=(TextView) findViewById(R.id.textViewSignUp);

        buttonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editTextUsername.getText().toString().equals("Karmela") && editTextPassword.getText().toString().equals("Melai")) {
                    Toast.makeText(getApplicationContext(), "Logged in", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Username and password do not match", Toast.LENGTH_LONG).show();
                }
            }
        });

        textViewSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getApplicationContext(), com.example.ross.navigationdrawer.SignUp.class);
                startActivity(intent);
            }
        });
    }
}

